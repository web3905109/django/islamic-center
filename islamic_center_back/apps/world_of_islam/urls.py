from django.urls import path
from .views import *

app_name = "apps.world_of_islam"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("audios/", AudiosView.as_view(), name="audios"),
    path("books/", BooksView.as_view(), name="books"),
    path("files/", FilesView.as_view(), name="files"),
    path("images/", ImagesView.as_view(), name="images"),
    path("videos/", VideosView.as_view(), name="videos"),
    path("videos/<str:pk>/", VideoView.as_view(), name="videos"),
]
