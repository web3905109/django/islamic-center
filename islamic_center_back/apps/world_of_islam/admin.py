from django.contrib import admin
from apps.world_of_islam.models import *
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from apps.base.admin import BaseAdmin


class AudioResource(resources.ModelResource):
    class Meta:
        model = Audio


class VideoResource(resources.ModelResource):
    class Meta:
        model = Video


class BookResource(resources.ModelResource):
    class Meta:
        model = Book


class ImageResource(resources.ModelResource):
    class Meta:
        model = Image


@admin.register(Audio)
class AudioAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = AudioResource
    list_display = (
        "_edit",
        "is_published",
        "id",
        "_file",
        "_cover",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )
    list_per_page = 10
    list_max_show_all = 100
    list_filter = ("created_date", "updated_date")
    date_hierarchy = "created_date"
    search_help_text = _("Search elements across all columns and rows.")
    search_fields = ("created_date",)


@admin.register(Video)
class VideoAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = VideoResource
    list_display = (
        "_edit",
        "is_published",
        "id",
        "_file",
        "_cover",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )
    list_per_page = 10
    list_max_show_all = 100
    list_filter = ("created_date", "updated_date")
    date_hierarchy = "created_date"
    search_help_text = _("Search elements across all columns and rows.")
    search_fields = ("created_date",)


@admin.register(Book)
class BookAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = BookResource
    list_display = (
        "_edit",
        "is_published",
        "id",
        "_title",
        "_file",
        "_cover",
        "language",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )
    date_hierarchy = "created_date"
    list_per_page = 10
    list_max_show_all = 100
    list_filter = ("created_date", "updated_date")
    search_help_text = _("Search elements across all columns and rows.")
    search_fields = ("created_date",)


@admin.register(Image)
class ImageAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = ImageResource
    list_display = (
        "_edit",
        "is_published",
        "id",
        "_image",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )
    list_per_page = 10
    list_max_show_all = 100
    list_filter = ("created_date", "updated_date")
    date_hierarchy = "created_date"
    search_help_text = _("Search elements across all columns and rows.")
    search_fields = ("created_date",)
