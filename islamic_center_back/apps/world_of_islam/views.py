from apps.base.views import BaseContext
from django.views.generic import TemplateView, View
from django.core.paginator import Paginator
from apps.world_of_islam.models import *
from django.utils.translation import get_language, gettext as _
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseBadRequest
from django.conf import settings
from django.db.models import F
from urllib.parse import quote
import mimetypes
import os


class IndexView(TemplateView, BaseContext):
    template_name = "world_of_islam/index.html"
    models = {
        "videos": Video,
        "audios": Audio,
        "books": Book,
        "images": Image,
    }
    fields = {
        model_name: {field.name for field in model._meta.get_fields()}
        for model_name, model in models.items()
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        for model_name, model in self.models.items():
            fields, changing_fields = self.filter_searching_fields(
                excluded_fields=["is_published", "updated_date"],
                all_fields=self.fields[model_name],
            )

            objects = (
                model.objects.filter(is_published=True)
                .order_by("-created_date")
                .values(*fields)[:10]
            )

            self.change_field_names(
                object_list=objects, changing_fields=changing_fields
            )

            context[model_name] = objects

        return context


class BaseView(BaseContext):
    paginator_class = Paginator
    paginate_by = 12
    model = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        all_fields = {field.name for field in self.model._meta.get_fields()}

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["is_published", "updated_date"],
            all_fields=all_fields,
        )

        category = self.request.path.split("/")[-2]
        query_params = self.request.GET.copy()
        context["order_type"] = query_params.get("order_type", "date")
        context["filter_type"] = query_params.get("filter_type", "")
        context[
            "query_params"
        ] = f"&{query_params.pop('page', True) and query_params.urlencode()}"

        order_type = self.set_order_type(
            order_type=context["order_type"],
            category=category,
        )

        if context["filter_type"] and "books" == category:
            objects = (
                self.model.objects.filter(
                    is_published=True, language=context["filter_type"]
                )
                .order_by(order_type)
                .values(*fields)
            )
        else:
            objects = (
                self.model.objects.filter(is_published=True)
                .order_by(order_type)
                .values(*fields)
            )

        self.change_field_names(object_list=objects, changing_fields=changing_fields)

        page = self.request.GET.get("page", 1)
        paginator = self.paginator_class(objects, self.paginate_by)

        try:
            context["objects"] = paginator.page(page)
            context["paginator"] = paginator.get_page(page)

        except:
            pass

        return context

    def set_order_type(self, order_type, category):
        if order_type == "title":
            if "books" != category:
                order_type = "title_" + get_language()
        elif order_type == "hit":
            order_type = "-hit"
        elif order_type == "author":
            order_type = F(order_type).asc(nulls_last=True)
        else:
            order_type = "-created_date"

        return order_type


class AudiosView(TemplateView, BaseView):
    template_name = "world_of_islam/audios.html"
    model = Audio

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["order_types"] = {
            "author": _("Author"),
            "date": _("Date"),
            "title": _("Title"),
            "hit": _("Downloads"),
        }

        return context


class BooksView(TemplateView, BaseView):
    template_name = "world_of_islam/books.html"
    model = Book

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["order_types"] = {
            "author": _("Author"),
            "date": _("Date"),
            "title": _("Title"),
            "hit": _("Downloads"),
        }
        context["book_lang"] = {
            "": _("All"),
            "en": _("English book"),
            "ru": _("Russian book"),
            "uz": _("Uzbek book"),
        }

        return context


class ImagesView(TemplateView, BaseView):
    template_name = "world_of_islam/images.html"
    paginate_by = 25
    model = Image


class VideosView(TemplateView, BaseView):
    template_name = "world_of_islam/videos.html"
    paginate_by = 5
    model = Video

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["order_types"] = {
            "date": _("Date"),
            "title": _("Title"),
            "hit": _("Views"),
        }

        return context


class VideoView(TemplateView, BaseView):
    all_fields = {field.name for field in Video._meta.get_fields()}
    template_name = "world_of_islam/video.html"
    model = Video

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["is_published", "updated_date"],
            all_fields=self.all_fields,
        )

        try:
            video = Video.objects.filter(
                is_published=True, id=self.request.path.split("/")[-2]
            )

            video.update(hit=video[0].hit + 1)
            video = [*video.values()]

            self.change_field_names(
                object_list=video,
                changing_fields=changing_fields,
            )

            context["video"] = video[0]

        except:
            pass

        context["paginator"] = None

        return context


class FilesView(View):
    def get(self, request):
        category = request.GET.get("category", "")
        file_id = request.GET.get("id", "")

        if not category or not file_id:
            return HttpResponseBadRequest("Bad request.")

        obj = None

        try:
            if category == "book":
                objs = Book.objects.filter(is_published=True, id=file_id)

            elif category == "audio":
                objs = Audio.objects.filter(is_published=True, id=file_id)

            elif category == "video":
                objs = Video.objects.filter(is_published=True, id=file_id)

            elif category == "image":
                objs = Image.objects.filter(is_published=True, id=file_id)
        except:
            pass

        obj = [*objs.values()][0]

        if not obj or not obj["file"]:
            return HttpResponseNotFound("Source not available.")

        lang = request.COOKIES.get("lang") or settings.LANGUAGE_CODE

        file_path = os.path.join(settings.BASE_DIR, "media", obj["file"])
        file = open(file_path, "rb")

        if category == "book":
            filename = obj["title"]
        elif lang == "ru":
            filename = obj["title_ru"]
        elif lang == "uz":
            filename = obj["title_uz"]
        else:
            filename = obj["title_en"]

        mimetype = mimetypes.guess_type(file_path)[0]
        extension = mimetypes.guess_extension(mimetype)

        response = HttpResponse()
        response.write(file.read())
        response["Content-Type"] = mimetype
        response["Content-Length"] = os.path.getsize(file_path)
        response[
            "Content-Disposition"
        ] = f"attachment; filename*=utf-8''{quote(filename + extension)}"

        file.close()
        objs.update(hit=obj["hit"] + 1)

        return response
