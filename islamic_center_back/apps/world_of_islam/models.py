from django.db import models
from django_resized import ResizedImageField
from django.utils.translation import gettext as _
from apps.base.models import (
    AuthorModel,
    TitleModel,
    TextModel,
    DateModel,
    FileClearModel,
)


class BaseModel(models.Model):
    cover = ResizedImageField(
        upload_to="world_of_islam/covers/",
        force_format="WEBP",
        quality=90,
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True


class Audio(TitleModel, AuthorModel, DateModel, FileClearModel, BaseModel):
    file = models.FileField(upload_to="world_of_islam/audios/", blank=False, null=False)


class Book(AuthorModel, DateModel, FileClearModel, BaseModel):
    title = models.CharField(max_length=250, blank=False, null=False)
    file = models.FileField(upload_to="world_of_islam/books/", blank=False, null=False)

    class Language(models.TextChoices):
        ENGLISH = ("en", "English book")
        RUSSIAN = ("ru", "Русская книга")
        UZBEK = ("uz", "O'zbekcha kitob")

    language = models.CharField(
        max_length=2,
        choices=Language.choices,
        blank=False,
        null=False
    )


class Video(TitleModel, TextModel, AuthorModel, DateModel, FileClearModel, BaseModel):
    file = models.FileField(upload_to="world_of_islam/videos/", blank=False, null=False)
    author = None


class Image(TitleModel, AuthorModel, DateModel, FileClearModel, BaseModel):
    cover = None
    image = ResizedImageField(
        upload_to="world_of_islam/images/",
        force_format="WEBP",
        blank=False,
        null=False,
        quality=90,
    )
