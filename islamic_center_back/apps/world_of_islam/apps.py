from django.apps import AppConfig


class WorldOfIslamConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.world_of_islam"
