from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from apps.base.admin import BaseAdmin
from apps.timetable.models import *


class TimeTableResource(resources.ModelResource):
    class Meta:
        model = TimeTable


@admin.register(TimeTable)
class TimeTableAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = TimeTableResource
    list_display = (
        "_edit",
        "id",
        "date",
        "fajr",
        "sunrise",
        "dhuhr",
        "asr",
        "maghrib",
        "isha",
    )
    list_filter = ("date",)
    list_per_page = 30
    list_max_show_all = 366
    date_hierarchy = "date"
    search_fields = ("date",)
