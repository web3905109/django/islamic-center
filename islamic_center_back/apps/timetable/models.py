from django.db import models


class TimeTable(models.Model):
    date = models.DateField(unique=True, blank=False, null=False)
    fajr = models.TimeField(blank=False, null=False)
    sunrise = models.TimeField(blank=False, null=False)
    dhuhr = models.TimeField(blank=False, null=False)
    asr = models.TimeField(blank=False, null=False)
    maghrib = models.TimeField(blank=False, null=False)
    isha = models.TimeField(blank=False, null=False)
