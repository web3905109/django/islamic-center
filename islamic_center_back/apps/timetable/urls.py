from django.urls import path
from .views import *

app_name = "apps.timetable"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("pdf/", PDFGeneratorView.as_view(), name="pdf"),
]
