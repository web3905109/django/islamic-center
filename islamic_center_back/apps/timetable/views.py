from django.views.generic import TemplateView
from apps.base.views import BaseContext
from apps.timetable.models import TimeTable
from django_xhtml2pdf.views import PdfMixin
from django.utils.translation import gettext as _
from django.utils.html import format_html
from datetime import datetime



class IndexView(TemplateView, BaseContext):
    template_name = "timetable/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            month = int(self.request.GET.get("month", datetime.now().month))

            if month > 12:
                month = 12
        except:
            month = datetime.now().month

        timetable = TimeTable.objects.filter(date__month=month).order_by("date")

        context["timetable"] = timetable
        context["months"] = [
            _("January"),
            _("February"),
            _("March"),
            _("April"),
            _("May"),
            _("June"),
            _("July"),
            _("August"),
            _("September"),
            _("October"),
            _("November"),
            _("December"),
        ]
        context["month_num"] = month
        context["month_name"] = context["months"][month - 1]
        context["table_title"] = format_html(
            _("Prayer calendar for %(month_name)s")
            % {"month_name": f"<strong>{context['month_name']}</strong>"}
        )

        return context


class PDFGeneratorView(PdfMixin, IndexView):
    template_name = "timetable/pdf.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["table_header"] = [
            _("Date"),
            _("Day of the week"),
            _("Fajr"),
            _("Sunrise"),
            _("Dhuhr"),
            _("Asr"),
            _("Maghrib"),
            _("Isha'a"),
        ]

        return context
