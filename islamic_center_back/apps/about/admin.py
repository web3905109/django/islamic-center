from django.contrib import admin
from apps.about.models import *
from django.utils.translation import gettext_lazy as _
from apps.base.admin import BaseAdmin


@admin.register(Founder)
class FounderAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(Mission)
class MissionAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(Overview)
class OverviewAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(Staff)
class StaffAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin, BaseAdmin):
    pass


@admin.register(Welcome)
class WelcomeAdmin(admin.ModelAdmin, BaseAdmin):
    pass
