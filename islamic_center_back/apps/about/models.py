from django.db import models
from django_resized import ResizedImageField
from apps.base.models import (
    TitleModel,
    SubTitleModel,
    TextModel,
    FileClearModel,
)


class ImageModel(models.Model):
    image = ResizedImageField(
        upload_to="about/images/",
        force_format="WEBP",
        blank=False,
        null=False,
        quality=90,
    )


class Welcome(TitleModel, ImageModel, FileClearModel):
    video = models.FileField(upload_to="about/videos/", blank=True, null=True)


class History(TitleModel, TextModel):
    pass


class Founder(TitleModel, SubTitleModel, TextModel, ImageModel):
    link_en = models.CharField(max_length=1000)
    link_ru = models.CharField(max_length=1000)
    link_uz = models.CharField(max_length=1000)


class Overview(TitleModel, TextModel, ImageModel):
    pass


class Mission(TitleModel, TextModel):
    pass


class Staff(TitleModel, SubTitleModel, TextModel, ImageModel):
    pass


class Team(TitleModel, TextModel):
    pass
