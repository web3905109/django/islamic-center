from apps.base.views import BaseContext
from django.views.generic import TemplateView
from apps.about.models import *

class IndexView(TemplateView, BaseContext):
    template_name = "about/index.html"
