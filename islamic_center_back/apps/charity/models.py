from django.db import models
from apps.base.models import TitleModel, TextModel


class DonationMethod(TitleModel, TextModel):
    link = models.CharField(max_length=1000, blank=True, null=True)
