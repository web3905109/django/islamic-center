from django.urls import path
from .views import *

app_name = "apps.charity"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
]
