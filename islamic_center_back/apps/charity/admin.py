from django.contrib import admin
from apps.base.admin import BaseAdmin
from apps.charity.models import *


@admin.register(DonationMethod)
class DonationMethodAdmin(admin.ModelAdmin, BaseAdmin):
    list_display = (
        "_edit",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "_text_en",
        "_text_ru",
        "_text_uz",
        "link",
    )
    list_per_page = 10
    list_max_show_all = 100
