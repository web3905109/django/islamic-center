from django.views.generic import TemplateView
from apps.base.views import BaseContext


class IndexView(TemplateView, BaseContext):
    template_name = "charity/index.html"
