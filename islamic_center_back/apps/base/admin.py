from django.contrib import admin
from apps.base.models import *
from django.utils.translation import gettext_lazy as _
from django.utils.html import format_html


class BaseAdmin:
    def _edit(self, obj):
        return format_html(
            f"<img src='/static/admin/img/icon-changelink.svg' alt='{_('Change')}' title='{_('Change')}' class='edit-button'/>"
        )

    def _author(self, obj):
        return self.text_transformatter(obj.author)

    def _title(self, obj):
        return self.text_transformatter(obj.title)

    def _title_en(self, obj):
        return self.text_transformatter(obj.title_en)

    def _title_ru(self, obj):
        return self.text_transformatter(obj.title_ru)

    def _title_uz(self, obj):
        return self.text_transformatter(obj.title_uz)

    def _sub_title_en(self, obj):
        return self.text_transformatter(obj.text_en)

    def _sub_title_ru(self, obj):
        return self.text_transformatter(obj.text_ru)

    def _sub_title_uz(self, obj):
        return self.text_transformatter(obj.text_uz)

    def _text_en(self, obj):
        return self.text_transformatter(obj.text_en)

    def _text_ru(self, obj):
        return self.text_transformatter(obj.text_ru)

    def _text_uz(self, obj):
        return self.text_transformatter(obj.text_uz)

    def _link(self, link, prefix="/media/"):
        return self.text_transformatter(
            format_html(
                "<a href='{}{}' style='font-style:italic;' target='_blank'>{}</a>",
                prefix,
                link,
                str(link).split("/", -1)[-1],
            )
            if link
            else format_html(f"<i>{_('None')}</i>")
        )

    def _file(self, obj):
        return self._link(obj.file)

    def _image(self, obj):
        return self._link(obj.image)

    def _audio(self, obj):
        return self._link(obj.audio)

    def _video(self, obj):
        return self._link(obj.video)

    def _cover(self, obj):
        return self._link(obj.cover)

    def text_transformatter(self, text):
        return (
            format_html(
                "<div style='display:inline-block;max-width:10rem;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;'>{}</div>",
                text,
            )
            if text
            else format_html(f"<i>{_('None')}</i>")
        )

    # class Meta:
    #     abstract = True


# @admin.register(UserDetails)
# class UserDetailsAdmin(admin.ModelAdmin, BaseModelAdmin):
#     list_display = (
#         "_edit",
#         "user",
#         "username",
#         "email",
#         "phone_number_personal",
#         "phone_number_work",
#         "type_of_work",
#         "residental_status",
#         "marital_status",
#         "number_of_children",
#         "type_of_membership",
#         "donation_amount",
#         "donation_frequency",
#         "donation_date",
#         "updated_date",
#         "id",
#     )
#     list_filter = (
#         "donation_date",
#         "updated_date",
#         "donation_amount",
#         "donation_frequency",
#         "type_of_membership",
#         "type_of_work",
#         "residental_status",
#         "marital_status",
#         "number_of_children",
#     )
#     list_per_page = 10
#     list_max_show_all = 100
#     date_hierarchy = "donation_date"
#     search_fields = (
#         "user",
#         "username",
#         "email",
#         "phone_number_personal",
#         "phone_number_work",
#         "type_of_work",
#         "residental_status",
#         "marital_status",
#         "number_of_children",
#         "type_of_membership",
#         "donation_amount",
#         "donation_frequency",
#         "donation_date",
#         "updated_date",
#         "id",
#     )
