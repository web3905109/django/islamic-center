from django import template
from django.utils.html import format_html
import random


register = template.Library()


@register.filter
def indent(value, arg):
    return format_html(
        value.replace("<br>", f"<br><span style='display:inline-block;text-indent:{arg};'>&nbsp;</span>")
    )

@register.simple_tag
def rand(a, b=None):
    if b is None:
        a, b = 0, a
    return random.randint(a, b)

@register.filter
def mul(value, arg):
    return int(value) * int(arg)
