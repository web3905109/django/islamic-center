from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin
from django.urls import reverse_lazy
from apps.news.models import News
from django.utils.translation import activate, get_language, gettext as _
from apps.contacts.models import Address
from django.conf import settings
import logging


class BaseContext(ContextMixin):
    langs = tuple(lang for lang, _ in settings.LANGUAGES)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        lang = self.request.COOKIES.get("lang")

        activate(lang or settings.LANGUAGE_CODE)
        logging.info(lang)

        context["address"] = Address.objects.first()

        return context

    def filter_searching_fields(self, all_fields, excluded_fields=[]):
        changing_fields = []
        fields = all_fields - set(
            [
                field
                if field.split("_")[-1] != get_language()
                else changing_fields.append(field)
                for field in all_fields
                if field.split("_")[-1] in self.langs
            ]
            + excluded_fields
        )

        return (fields, changing_fields)

    def change_field_names(self, object_list=[], changing_fields=[]):
        for news in object_list:
            for field in changing_fields:
                news[field[:-3]] = news.pop(field)

        return object_list


class CustomLoginView(LoginView, BaseContext):
    template_name = "base/auth/login.html"
    fields = "__all__"
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy("apps.base:index")


class IndexView(TemplateView, BaseContext):
    all_fields = {field.name for field in News._meta.get_fields()}
    template_name = "base/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["is_published", "video", "updated_date"],
            all_fields=self.all_fields,
        )

        news_list = (
            News.objects.filter(is_published=True)
            .order_by("-created_date")
            .values(*fields)[:10]
        )

        self.change_field_names(object_list=news_list, changing_fields=changing_fields)

        context["news_list"] = news_list

        return context
