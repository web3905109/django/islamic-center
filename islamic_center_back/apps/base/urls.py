from django.urls import path
from .views import *
from django.contrib.auth.views import LogoutView

app_name = "apps.base"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("login/", CustomLoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(next_page="apps.base:index"), name="logout"),
]
