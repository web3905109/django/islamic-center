from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _

# from django.contrib.auth.models import User


class TitleModel(models.Model):
    title_en = models.CharField(max_length=250, blank=False, null=False)
    title_ru = models.CharField(max_length=250, blank=False, null=False)
    title_uz = models.CharField(max_length=250, blank=False, null=False)

    def __str__(self):
        return _(self.title_en)

    class Meta:
        abstract = True


class SubTitleModel(models.Model):
    sub_title_en = models.CharField(max_length=250, blank=True, null=True)
    sub_title_ru = models.CharField(max_length=250, blank=True, null=True)
    sub_title_uz = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        abstract = True


class TextModel(models.Model):
    text_en = models.TextField(blank=False, null=False)
    text_ru = models.TextField(blank=False, null=False)
    text_uz = models.TextField(blank=False, null=False)

    class Meta:
        abstract = True


class AuthorModel(models.Model):
    author = models.CharField(max_length=50, blank=True, null=True)
    hit = models.IntegerField(default=1)
    is_published = models.BooleanField(default=True)

    class Meta:
        abstract = True


class DateModel(models.Model):
    created_date = models.DateTimeField(default=timezone.now, auto_now_add=False)
    updated_date = models.DateTimeField(auto_now=True, verbose_name="updated_date")

    class Meta:
        abstract = True


class FileClearModel:
    pass
    # def _do_update(self, *args, **kwargs):
    #     news_old = self._meta.model.objects.get(id=self.id)
    #     fields_old = [str(news_old.image), str(news_old.video)]

    #     news_new = super()._do_update(*args, **kwargs)
    #     fields_new = [str(self.image), str(self.video)]

    #     for i in range(len(fields_old)):
    #         if fields_old[i] and fields_old[i] != fields_new[i]:
    #             file = os.path.join(settings.MEDIA_ROOT, fields_old[i])

    #             if os.path.exists(file):
    #                 os.remove(file)

    #     return news_new

    # def delete(self, *args, **kwargs):
    #     fileds = [str(self.image), str(self.video)]
    #     result = super().delete(*args, **kwargs)

    #     for field in fileds:
    #         if field:
    #             file = os.path.join(settings.MEDIA_ROOT, field)

    #             if os.path.exists(file):
    #                 os.remove(file)

    #     return result


# class UserDetails(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     email = models.CharField(max_length=1000)
#     username = models.CharField(max_length=1000)
#     phone_number_personal = models.CharField(max_length=1000)
#     phone_number_work = models.CharField(max_length=1000)
#     marital_status = models.CharField(max_length=1000)
#     number_of_children = models.CharField(max_length=1000)
#     residental_status = models.CharField(max_length=1000)
#     type_of_work = models.CharField(max_length=1000)
#     type_of_membership = models.CharField(max_length=1000)
#     donation_amount = models.IntegerField()
#     donation_frequency = models.CharField(max_length=1000)
#     donation_date = models.DateTimeField(
#         auto_now_add=True, verbose_name="donation_date"
#     )
#     updated_date = models.DateTimeField(auto_now=True, verbose_name="updated_date")
