from django.db import models
from apps.base.models import TitleModel, TextModel, SubTitleModel


class Information(TitleModel, SubTitleModel, TextModel):
    fontawesome_free_icon = models.CharField(max_length=20, blank=False, null=False)
    icon_color_hex = models.CharField(max_length=8, blank=False, null=False)
