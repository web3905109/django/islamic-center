from django.contrib import admin
from apps.base.admin import BaseAdmin
from apps.contacts.models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class NewsResource(resources.ModelResource):
    class Meta:
        model = Employee


@admin.register(Employee)
class EmployeeAdmin(ImportExportModelAdmin, BaseAdmin):
    list_display = (
        "_edit",
        "id",
        "status",
        "contact_name",
        "position_en",
        "position_ru",
        "position_uz",
        "email",
        "phone",
    )
    list_per_page = 10
    list_max_show_all = 100


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin, BaseAdmin):
    list_display = (
        "_edit",
        "address_line_1",
        "address_line_2",
        "_location_link",
        "email",
        "phone",
    )
    list_per_page = 10
    list_max_show_all = 100

    def _location_link(self, obj):
        return self.text_transformatter(obj.location_link)
