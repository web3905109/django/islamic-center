from django.views.generic import TemplateView
from apps.base.views import BaseContext
from apps.contacts.models import Employee


class IndexView(TemplateView, BaseContext):
    all_fields = {field.name for field in Employee._meta.get_fields()}
    template_name = "contacts/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["status", "id"],
            all_fields=self.all_fields,
        )

        employees = Employee.objects.filter(status=True).values(*fields)

        self.change_field_names(
                object_list=employees,
                changing_fields=changing_fields,
            )
        
        context["employees"] = employees

        return context
