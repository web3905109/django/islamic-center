from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Employee(models.Model):
    contact_name = models.CharField(max_length=150, blank=False, null=False)
    position_en = models.CharField(max_length=75, blank=False, null=False)
    position_ru = models.CharField(max_length=75, blank=False, null=False)
    position_uz = models.CharField(max_length=75, blank=False, null=False)
    status = models.BooleanField(default=True)
    email = models.EmailField(blank=False, null=False)
    phone = PhoneNumberField()

class Address(models.Model):
    address_line_1 = models.CharField(max_length=100, blank=False, null=False)
    address_line_2 = models.CharField(max_length=100, blank=False, null=False)
    location_link = models.CharField(max_length=1000, blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    phone = PhoneNumberField()
    working_time = models.CharField(max_length=20, blank=False, null=False)
    working_days = models.CharField(max_length=40, blank=False, null=False)
"""
8665 State Route 48
Maineville, Ohio 45039
https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3087.258236722717!2d-84.21567721055754!3d39.30506956266425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8840f9a6364311ad%3A0xb80ac28a9d358555!2s8665%20OH-48%2C%20Maineville%2C%20OH%2045039!5e0!3m2!1sen!2sus!4v1699792443547!5m2!1sen!2sus
icmoh2020@gmail.com
+998901234567
9:00 - 18:00
Monday - Sunday
"""