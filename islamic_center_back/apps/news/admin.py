from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from apps.base.admin import BaseAdmin
from apps.news.models import *
from django.utils.translation import gettext as _


class NewsResource(resources.ModelResource):
    class Meta:
        model = News


@admin.register(News)
class NewsAdmin(ImportExportModelAdmin, BaseAdmin):
    resource_class = NewsResource
    list_display = (
        "_edit",
        "is_published",
        "id",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "_text_en",
        "_text_ru",
        "_text_uz",
        "_image",
        "_video",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )
    date_hierarchy = "created_date"
    list_filter = ("created_date", "updated_date")
    list_per_page = 10
    list_max_show_all = 100
    search_help_text = _("Search elements across all columns and rows.")
    search_fields = (
        "id",
        "is_published",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "_text_en",
        "_text_ru",
        "_text_uz",
        "_image",
        "_video",
        "hit",
        "_author",
        "created_date",
        "updated_date",
    )

    # def delete_queryset(self, request, queryset):
    #     for obj in queryset:
    #         obj.delete()

    # class Media:
    #     css = {"all": ("css/admin.css",)}
