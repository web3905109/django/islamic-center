from django.views.generic import TemplateView
from django.core.paginator import Paginator
from apps.base.views import BaseContext
from apps.news.models import News
from django.utils.translation import get_language, gettext as _
from django.db.models import F


class NewsDetailView(TemplateView, BaseContext):
    all_fields = {field.name for field in News._meta.get_fields()}
    template_name = "news/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["is_published", "updated_date"],
            all_fields=self.all_fields,
        )

        try:
            news = News.objects.filter(
                is_published=True, id=self.request.path.split("/")[-2]
            )

            news.update(hit=news[0].hit + 1)
            news = [*news.values()]

            self.change_field_names(
                object_list=news,
                changing_fields=changing_fields,
            )

            context["news"] = news[0]

        except:
            pass

        return context


class IndexView(TemplateView, BaseContext):
    all_fields = {field.name for field in News._meta.get_fields()}
    template_name = "news/index.html"
    paginator_class = Paginator
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        fields, changing_fields = self.filter_searching_fields(
            excluded_fields=["is_published", "video", "updated_date"],
            all_fields=self.all_fields,
        )

        query_params = self.request.GET.copy()
        context["order_type"] = query_params.get("order_type", "date")
        context[
            "query_params"
        ] = f"&{query_params.pop('page', True) and query_params.urlencode()}"

        order_type = self.set_order_type(order_type=context["order_type"])

        news_list = (
            News.objects.filter(is_published=True).order_by(order_type).values(*fields)
        )

        self.change_field_names(object_list=news_list, changing_fields=changing_fields)

        page = self.request.GET.get("page", 1)
        paginator = self.paginator_class(news_list, self.paginate_by)

        try:
            context["news_list"] = paginator.page(page)
            context["paginator"] = paginator.get_page(page)
            context["order_types"] = {
                "author": _("Author"),
                "date": _("Date"),
                "title": _("Title"),
                "hit": _("Views"),
            }

        except:
            pass

        return context

    def set_order_type(self, order_type):
        if order_type == "title":
            order_type = "title_" + get_language()
        elif order_type == "hit":
            order_type = "-hit"
        elif order_type == "author":
            order_type = F(order_type).asc(nulls_last=True)
        else:
            order_type = "-created_date"

        return order_type
