from django.db import models
from django_resized import ResizedImageField
from apps.base.models import (
    TitleModel,
    TextModel,
    AuthorModel,
    DateModel,
    FileClearModel,
)


class News(TitleModel, TextModel, AuthorModel, DateModel, FileClearModel):
    video = models.FileField(upload_to="news/videos/", blank=True, null=True)
    image = ResizedImageField(
        upload_to="news/images/",
        force_format="WEBP",
        quality=90,
        blank=True,
        null=True,
    )
