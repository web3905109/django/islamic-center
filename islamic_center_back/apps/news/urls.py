from django.urls import path
from .views import *

app_name = "apps.news"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("<str:pk>/", NewsDetailView.as_view(), name="detail"),
]
