from django.contrib import admin
from apps.base.admin import BaseAdmin
from apps.museum.models import *


@admin.register(Museum)
class MuseumAdmin(admin.ModelAdmin, BaseAdmin):
    list_display = (
        "_edit",
        "id",
        "_title_en",
        "_title_ru",
        "_title_uz",
        "_sub_title_en",
        "_sub_title_ru",
        "_sub_title_uz",
        "_text_en",
        "_text_ru",
        "_text_uz",
    )
    list_per_page = 10
    list_max_show_all = 100
