"""islamic_center_back URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import gettext_lazy as _

admin.site.index_title = _("Administration")
admin.site.site_title = _("Islamic Center of Maineville")
admin.site.site_header = _("Islamic Center of Maineville Administration")

urlpatterns = i18n_patterns(path("more-secret-admin/", admin.site.urls))
urlpatterns += (
    [
        path("", include("apps.base.urls")),
        path("about/", include("apps.about.urls")),
        path("charity/", include("apps.charity.urls")),
        path("contacts/", include("apps.contacts.urls")),
        path("member/", include("apps.member.urls")),
        path("museum/", include("apps.museum.urls")),
        path("news/", include("apps.news.urls")),
        path("timetable/", include("apps.timetable.urls")),
        path("world_of_islam/", include("apps.world_of_islam.urls")),
    ]
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
